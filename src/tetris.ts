'use strict';

import { Piece } from "./piece";
import { BlockPiece } from "./pieces/block-piece";
import { LInvertedPiece } from "./pieces/l-inverted-piece";
import { LPiece } from "./pieces/l-piece";
import { LongPiece } from "./pieces/long-piece";
import { SInvertedPiece } from "./pieces/s-inverted-piece";
import { SPiece } from "./pieces/s-piece";

export class Tetris {
    private width: number;
    private height: number;
    private screen: number[];
    private graveyard: number[];
    private pieces: Piece[];
    private nextPiece: Piece;
    private lastUpdate: number = 0;
    private initialSpeed: number = 800;
    private speed: number = this.initialSpeed;
    private speedLimit: number = 100;
    private speedRate: number = 0.1;
    
    constructor (width: number, height: number) {
        this.width = width;
        this.height = height;
        this.pieces = [];
        this.screen = new Array(width * height).fill(0);
        this.graveyard = new Array(width * height).fill(0);

        this.nextPiece = this.getRandomPiece();
        this.startGame();

        setInterval(() => this.update(), 33);
    }

    public moveActivePieceLeft() {
        const activePiece: Piece | undefined = this.pieces.find((piece: Piece) => piece.active);
        if (!activePiece) return;
        if (this.tryMoveLeft(activePiece)) {
            activePiece.x -= 1;
        }
    }

    public moveActivePieceRight() {
        const activePiece: Piece | undefined = this.pieces.find((piece: Piece) => piece.active);
        if (!activePiece) return;
        if (this.tryMoveRight(activePiece)) {
            activePiece.x += 1;
        }
    }

    public moveActivePieceDown() {
        const activePiece: Piece | undefined = this.pieces.find((piece: Piece) => piece.active);
        if (!activePiece) return;
        while (this.tryMoveDown(activePiece)) {
            activePiece.y += 1;
        }
    }

    public rotateActivePiece() {
        const activePiece: Piece | undefined = this.pieces.find((piece: Piece) => piece.active);
        if (!activePiece) return;
        const originalX: number = activePiece.x;
        for (let drag = 0; drag < activePiece.width - 1; drag += 1) {
            if (!this.checkPreviewContact(activePiece)) {
                activePiece.rotate();
                return;
            }
            activePiece.x - 1;
        }
        activePiece.x = originalX;
    }

    public getScreen() {
        return this.screen;
    }

    private getRandomPiece(): Piece {
        const random = Math.floor(Math.random() * 6);
        if (random === 0) return new BlockPiece();
        if (random === 1) return new LInvertedPiece();
        if (random === 2) return new LPiece();
        if (random === 3) return new LongPiece();
        if (random === 4) return new SInvertedPiece();
        return new SPiece();
    }

    private spawnPiece(piece: Piece) {
        piece.x = 5 - (Math.floor(piece.width / 2));
        piece.y = piece.height - 1;
        for (const r of new Array(Math.floor(Math.random() * 4)).fill(0)) piece.rotate();
        piece.active = true;
        if (this.checkContact(piece)) {
            this.pieces = [];
            this.startGame();
        } else {
            this.pieces.push(piece);
        }
        this.nextPiece = this.getRandomPiece();
    }

    private startGame() {
        this.screen.fill(0);
        this.graveyard.fill(0);

        this.speed = this.initialSpeed;

        // Draw vertical divisory at 11th pixel
        this.screen = this.screen.map((pixel: number, index: number) => index % 16 === 10 ? 1 : 0);

        this.spawnPiece(this.nextPiece);
    }

    private convertXYToIndex(x: number, y: number): number {
        return (y * 16) + x;
    }

    private checkPreviewContact(piece: Piece): boolean {
        const previewPattern: number[][] = piece.previewRotation.reverse();
        const currentPattern: number[][] = piece.currentPattern.reverse();
        const width: number = previewPattern[0].length;
        const height: number = previewPattern.length;

        for (let y: number = 0; y < height; y += 1) {
            for (let x: number = 0; x < width; x += 1) {
                if (previewPattern[y][x] === 0) continue;
                if (y < piece.height && x < piece.width && currentPattern[y][x] > 0) continue;
                if (this.screen[this.convertXYToIndex(piece.x + x, piece.y - y)] > 0) return true;
            }
        }
        return false;
    }

    private checkContact(piece: Piece): boolean {
        const currentPattern: number[][] = piece.currentPattern.reverse();

        for (let y: number = 0; y < piece.height; y += 1) {
            for (let x: number = 0; x < piece.width; x += 1) {
                if (currentPattern[y][x] === 0) continue;
                if (this.screen[this.convertXYToIndex(piece.x + x, piece.y - y)] > 0) return true;
            }
        }
        return false;
    }

    private drawPatternOnScreen(piece: Piece) {
        const currentPattern: number[][] = piece.currentPattern.reverse();

        for (let y: number = 0; y < piece.height; y += 1) {
            for (let x: number = 0; x < piece.width; x += 1) {
                if (currentPattern[y][x] === 0) continue;
                this.screen[this.convertXYToIndex(piece.x + x, piece.y - y)] = piece.color;
            }
        }
    }

    private drawPatternOnGraveyard(piece: Piece) {
        const currentPattern: number[][] = piece.currentPattern.reverse();

        for (let y: number = 0; y < piece.height; y += 1) {
            for (let x: number = 0; x < piece.width; x += 1) {
                if (currentPattern[y][x] === 0) continue;
                this.graveyard[this.convertXYToIndex(piece.x + x, piece.y - y)] = piece.color;
            }
        }
    }

    private tryMoveLeft(piece: Piece): boolean {
        if (piece.x === 0) return false;
        const currentPattern: number[][] = piece.currentPattern.reverse();

        for (let y: number = 0; y < piece.height; y += 1) {
            for (let x: number = 0; x < piece.width; x += 1) {
                if (currentPattern[y][x] === 0) continue;
                if (x > 0 && currentPattern[y][x - 1] > 0) continue;
                if (this.screen[this.convertXYToIndex(piece.x + x - 1, piece.y - y)] > 0) {
                    return false;
                }
            }
        }
        return true;
    }

    private tryMoveRight(piece: Piece): boolean {
        if (piece.x + piece.width === 10) return false;
        const currentPattern: number[][] = piece.currentPattern.reverse();

        for (let y: number = 0; y < piece.height; y += 1) {
            for (let x: number = 0; x < piece.width; x += 1) {
                if (currentPattern[y][x] === 0) continue;
                if (x < (piece.width - 1) && currentPattern[y][x + 1] > 0) continue;
                if (this.screen[this.convertXYToIndex(piece.x + x + 1, piece.y - y)] > 0) {
                    return false;
                }
            }
        }
        return true;
    }

    private tryMoveDown(piece: Piece): boolean {
        if (piece.y === this.height - 1) return false;
        const currentPattern: number[][] = piece.currentPattern.reverse();

        for (let y: number = 0; y < piece.height; y += 1) {
            for (let x: number = 0; x < piece.width; x += 1) {
                if (currentPattern[y][x] === 0) continue;
                if (y > 0 && currentPattern[y - 1][x] > 0) continue;
                if (this.screen[this.convertXYToIndex(piece.x + x, piece.y - y + 1)] > 0) {
                    return false;
                }
            }
        }
        return true;
    }

    private checkPoint(): void {
        for (let row: number = 0; row < this.graveyard.length / this.width; row += 1) {
            if (this.graveyard.slice(row * 16, (row * 16) + 10).indexOf(0) < 0) {
                for (let newRow: number = row; newRow >= 0; newRow -= 1) {
                    if (newRow > 0) {
                        this.graveyard.splice(newRow * 16, 10, ... this.graveyard.slice((newRow - 1) * 16, ((newRow - 1) * 16) + 10));
                    } else {
                        this.graveyard.splice(0, 10, ...new Array(10).fill(0));
                    }
                }
            }
        }
    }

    public update() {
        this.speed -= this.speedRate;
        if (this.speed < this.speedLimit) this.speed = this.speedLimit;

        this.screen.fill(0);

        // Draw vertical divisory at 11th pixel
        this.screen = this.screen.map((pixel: number, index: number) => index % 16 === 10 ? 1 : 0);

        // Drawgraveyard
        this.screen = this.screen.map((pixel: number, index: number) => this.graveyard[index] > 0 ? this.graveyard[index] : this.screen[index]);

        // Draw pieces
        for (const piece of this.pieces) {
            if (piece.active && Date.now() - this.lastUpdate > this.speed) {
                this.lastUpdate = Date.now();
                if (this.tryMoveDown(piece)) {
                    piece.y += 1;
                } else {
                    piece.active = false;
                    this.drawPatternOnGraveyard(piece);
                    this.pieces = this.pieces.filter((p: Piece) => p !== piece);
                    this.spawnPiece(this.nextPiece);
                }
            }
            this.drawPatternOnScreen(piece);
        }

        this.checkPoint();

        // Draw next piece
        if (this.nextPiece) {
            this.nextPiece.x = 13 - (Math.floor(this.nextPiece.width / 2));
            this.nextPiece.y = this.nextPiece.height;
            this.drawPatternOnScreen(this.nextPiece);
        }
    }
}
