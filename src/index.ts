'use strict';

require('dotenv').config();

import express from 'express';
import { SetupRoutes } from './routes';
import { Tetris } from './tetris';

const app = express();
app.use('/visualizer', express.static('visualizer'));

const tetris = new Tetris(parseInt(process.env.SCREEN_WIDTH ?? '16'), parseInt(process.env.SCREEN_HEIGHT ?? '16'));
SetupRoutes(app, tetris);

app.listen(process.env.PORT);
console.log(`Server started at port ${process.env.PORT}`);
