'use strict';

import express from 'express';
import { Tetris } from './tetris';

export function SetupRoutes(app: express.Express, tetris: Tetris) {
    app.get('/screen', (req, res) => {
        res.status(200).send(tetris.getScreen().join(''));
    });

    app.post('/left', (req, res) => {
        tetris.moveActivePieceLeft();
        res.status(200).send('');
    });

    app.post('/right', (req, res) => {
        tetris.moveActivePieceRight();
        res.status(200).send('');
    });

    app.post('/rotate', (req, res) => {
        tetris.rotateActivePiece();
        res.status(200).send('');
    });

    app.post('/down', (req, res) => {
        tetris.moveActivePieceDown();
        res.status(200).send('');
    });
}
