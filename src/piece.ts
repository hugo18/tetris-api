'use strict';

export abstract class Piece {
    private pattern: number[][];
    private rotation: number = 0;
    public x: number;
    public y: number;
    public color: number;
    public active: boolean;

    constructor(pattern: number[][]) {
        if (pattern.length === 0 || pattern[0].length === 0 || pattern.find((p: number[]) => p.length !== pattern[0].length)) {
            console.error('Invalid Pattern', pattern);
            throw 'Invalid Pattern';
        }
        this.pattern = pattern;
        this.x = 0;
        this.y = 0;
        this.color = 2 + Math.floor(Math.random() * 6);
        this.active = false;
    }

    public get width(): number {
        return this.currentPattern[0].length;
    }

    public get height(): number {
        return this.currentPattern.length;
    }

    public rotate() {
        this.rotation += 1;
        this.rotation = this.rotation % 4;
    }

    public get currentPattern(): number[][] {
        const rotation = Math.floor(this.rotation % 4);
        const transpose = (input: any[][]) => input[0].map((x: any, i: number) => input.map(x => x[i]));
        let p: number[][] = JSON.parse(JSON.stringify(this.pattern));
        for (let i: number = 0; i < rotation; i += 1) {
            p = transpose(p).map((row: number[]) => row.reverse());
        }
        return p;
    }

    public get previewRotation(): number[][] {
        const rotation = Math.floor(this.rotation % 4) + 1;
        const transpose = (input: any[][]) => input[0].map((x: any, i: number) => input.map(x => x[i]));
        let p: number[][] = JSON.parse(JSON.stringify(this.pattern));
        for (let i: number = 0; i < rotation; i += 1) {
            p = transpose(p).map((row: number[]) => row.reverse());
        }
        return p;
    }
}
