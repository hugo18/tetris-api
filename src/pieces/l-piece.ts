'use strict';

import { Piece } from "../piece";

export class LPiece extends Piece {
    constructor() {
        super(
            [
                [1, 0],
                [1, 0],
                [1, 1],
            ]
        );
    }
}