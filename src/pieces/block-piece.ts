'use strict';

import { Piece } from "../piece";

export class BlockPiece extends Piece {
    constructor() {
        super(
            [
                [1, 1],
                [1, 1],
            ]
        );
    }
}