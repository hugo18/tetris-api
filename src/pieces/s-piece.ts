'use strict';

import { Piece } from "../piece";

export class SPiece extends Piece {
    constructor() {
        super(
            [
                [0, 1, 1],
                [1, 1, 0],
            ]
        );
    }
}