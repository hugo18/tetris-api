'use strict';

import { Piece } from "../piece";

export class LInvertedPiece extends Piece {
    constructor() {
        super(
            [
                [0, 1],
                [0, 1],
                [1, 1],
            ]
        );
    }
}