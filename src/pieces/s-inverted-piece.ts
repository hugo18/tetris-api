'use strict';

import { Piece } from "../piece";

export class SInvertedPiece extends Piece {
    constructor() {
        super(
            [
                [1, 1, 0],
                [0, 1, 1],
            ]
        );
    }
}