'use strict';

import { Piece } from "../piece";

export class LongPiece extends Piece {
    constructor() {
        super(
            [
                [1],
                [1],
                [1],
                [1],
            ]
        );
    }
}